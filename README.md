<h1 align="center">
  Aplicação de Climas
</h1>

## 💻 Projeto

Este projeto é um projeto de estudo onde crio um site de aplicação de clima com API "OpenWeather API" utilizando HTML,CSS e JavaScript.

## 🚀 Tecnologias

- HTML
- CSS
- JavaScript

## 💻 Browse the site

https://aplicacaodeclimas.netlify.app/
